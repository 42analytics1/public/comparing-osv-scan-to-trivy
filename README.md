# Comparison of OSV-scanner to Trivy 

Google has released OSV-Scanner for vulnerability scanning.
42Analytics has been using Trivy for vulnerability scanning _and_ other security scanning, such as misconfigurations and secret leakage.
We compare the newly releases OSV-Scanner to Trivy for our particular use case for security both on paper and in a test case.
The main takeaway: do **NOT** rely on OSV-Scanner to find vulnerabilities in your Dockerized app's dependencies! :exclamation:

## Summary

In-practice comparison:
- :exclamation: **OSV-Scanner does not find vulnerable Python packages installed in a Docker image (likely similar for other language-specific packages)**
- OSV-Scanner finds markedly fewer lower-level library vulnerabilities than Trivy

On-paper comparison
- OSV-Scanner only supports vulnerability scanning, whereas Trivy also supports other kinds of security scanning such as configuration issues and secret scanning
- OSV-Scanner and Trivy both support various scan targets such as filesystem, lockfiles, Docker images
- OSV-Scanner and Trivy support dependency scanning from various language-specific lockfiles, with large overlap and some tool-specific lockfiles support
- OSV-Scanner is less configurable than Trivy
- OSV-Scanner supports fewer scan result output formats than Trivy

## Our use case
At 42Analytics, we scan the products we deliver on security issues.
Typically, we deliver products as Docker images.
To harden our Docker images, we apply a two-step approach

1. Scan the git repository before building the Docker image
   1. Assesses the security of the product requirements such as those defined in `requirements.txt` (Python)
   2. Assesses whether there are any security misconfigurations in the `Dockerimage` 
   3. Assesses whether there are any secrets present in the repository that are accidentally exposed
2. Scan the built Docker image this assesses the security of the dependencies of the "system" libraries upon which the app runs
   1. Assess the security of all other dependencies in the container such as system libraries
   2. Assess whether secrets have ended up in the container

# On-paper comparisons 

## Types of security scanning

Trivy supports not only vulnerability scanning, but also scanning for the presence of secrets, misconfiguration and licenses.

| Functionality                     | Trivy | OSV-Scanner           |
|-----------------------------------|-------|--------------------|
| Vulnerability scanning            | :white_check_mark: | :white_check_mark: |
| Secrets scanning                  | :white_check_mark: | :x:                |
| Configuration scanning            | :white_check_mark: | :x:                |
| License scanning                  | :white_check_mark: | :x:                |

Not really fair comparison if OSV-Scanner is only _supposed_ to do vulnerability scanning.
However, for our use case, it is nice to have it all in a single tool (Trivy).

## Scan targets
What can these scanners scan?

| Target            | Trivy              | OSV-Scanner           |
|-------------------|--------------------|--------------------|
| Docker image      | :white_check_mark: | :white_check_mark: |
| Local filesystem  | :white_check_mark: | :white_check_mark: |
| Remote repository | :white_check_mark: | :grey_question:\*  |
| RootFS            | :white_check_mark: | :x:                |
| SBOM              | :white_check_mark: | :white_check_mark: |

\* OSV-scanner can scan based on a commit ID; however it is unclear from the documentation what is meant by this.

For our purposes, filesystem scanning and Docker image scanning is sufficient.


## Lockfile support
Both scanners support a wide range of lockfiles that can be scanned for dependency vulnerabilities.

| Lockfile                            | Trivy              | OSV-Scanner           |
|-------------------------------------|--------------------|--------------------|
| `Cargo.lock` (Rust)                 | :white_check_mark: | :white_check_mark: |
| `composer.lock` (PHP)               | :white_check_mark: | :white_check_mark: |
| `conan.lock` (C/C++)                | :white_check_mark: | :x:                |
| `gemfile.lock` (Ruby)               | :white_check_mark: | :white_check_mark: |
| `go.mod` (Go)                       | :white_check_mark: | :x:                |
| `go.sum` (Go)                       | :white_check_mark: | :x:                |
| `mix.lock` (Elixir)                 | :x:                | :white_check_mark: 
| `package-lock.json` (Node.js)       | :white_check_mark: | :white_check_mark: |
| `packages.config` (NuGet)           | :white_check_mark: | :x:                |
| `packages.lock.json` (NuGet)        | :white_check_mark: | :x:                |
| `Pipefile.lock` (Python)            | :white_check_mark: | :white_check_mark: 
| `pnpm-lock.yaml` (NPM)              | :white_check_mark: | :white_check_mark: |
| `Podfile.lock` (Swift, Objective C) | :white_check_mark: | :x:                |
| `poetry.lock` (Python)              | :white_check_mark: | :white_check_mark: |
| `pom.xml` (Maven)                   | :white_check_mark: | :white_check_mark: |
| `pubspec.lock`  (Flutter)           | :x:                | :white_check_mark: |
| `renv.lock` (R)                     | :x:                | :x:                | 
| `requirements.txt` (Python)         | :white_check_mark: | :white_check_mark: |
| `yarn.lock` (Yarn)                  | :white_check_mark: | :white_check_mark: |

Common package ecosystems are supported by both Trivy and OSV-Scanner. 
Depending on what you develop in, you may need one or the other for vulnerability scanning.
We typically develop in Python and R.
Unfortunately, R (with `renv.lock`) is not supported by either.

## Scan result output information 

## Scan result formats 

Supported output formats of scan results. 

| Functionality | Trivy | OSV-Scanner           |
|---------------|-------|--------------------|
| Table         | :white_check_mark: | :white_check_mark: |
| JSON          | :white_check_mark: | :white_check_mark: |
| SARIF         | :white_check_mark: | :x:                |
| Custom        | :white_check_mark: | :x:                |

For our use case, output formats don't matter so much.
We have automated security policies in place that will notify us when there are actionable security issues.


## Scanning configuration options

| Configuration                      | Trivy                                                                                                              | OSV-Scanner           |
|------------------------------------|--------------------------------------------------------------------------------------------------------------------|--------------------|
| Configure by configuration file    | :white_check_mark:                                                                                                 | :white_check_mark: |
| Configure by environment variables | :white_check_mark:                                                                                                 | :x:                |
| Ignore vulnerabilities by ID       | :white_check_mark:                                                                                                 | :white_check_mark: |
| Ignore unfixed vulnerabilities     | :white_check_mark:                                                                                                 | :grey_question:    |
| Set minimum vulnerability score    | :white_check_mark:                                                                                                 | :x:                |
| Many, many more                    | :white_check_mark: ([here](https://aquasecurity.github.io/trivy/v0.35/docs/references/customization/config-file/)) | :x:                |

Scans can be configured both in Trivy and in OSV-Scanner.
For us, it is desirable that unfixed vulnerabilities (non-actionable vulnerabilities) do not break our builds. 
It is unclear whether OSV-scan reports on unfixed vulnerabilities.
In addition, it is desirable to only be alerted of vulnerabilities with a mimimum severity score.

# In-practice comparisons

We only compare Trivy and OSV-Scanner in terms of vulnerability scanning, even though Trivy offers other kinds of security scanning.
We create a Python project in the same way we would normally do.

## Test case
We define an empty Python app that we pretend requires Django 4.1, which has a known severe vulnerability.
We then dockerize the Python project by installing the app requirements into the Docker container using base image `python:3.10-slim-buster`, without applying system library updates.
This gives us two test cases:

1. Scanning based on the `requirements.txt`, the lockfile that defines Django 4.1 as a dependency
2. Scanning of the image (dockerized app)

The `requirements.txt`:
```
Django == 4.1
```

The `Dockerfile`:
```Docker
FROM python:3.10-slim-buster

COPY requirements.txt .
RUN pip install -r requirements.txt
```

```bash
# Build the image
docker build . -t test_image_insecure
```

## Results
We run the command-line versions of both OSV-Scanner and Trivy to perform vulnerability scanning and inspect the output to `stdout`.

### Lockfile scan

```bash
# OSV-Scanner
osv-scanner --lockfile requirements.txt

# Trivy
trivy filesystem --security-checks=vuln .
```

| Vulnerable library | Trivy              | OSV-Scanner           |
|--------------------|--------------------|--------------------|
| Django 4.1         | :white_check_mark: | :white_check_mark: |

### Docker image scan

```bash
# OSV-Scanner
osv-scanner --docker test_image_insecure

# Trivy (a): no filtering
trivy image --security-checks=vuln test_image_insecure

# Trivy (b): ignore unfixed
trivy image --security-checks=vuln --ignore-unfixed test_image_insecure

# Trivy (c): ignore unfixed, severity >= HIGH
trivy image --security-checks=vuln --ignore-unfixed --severity=HIGH,CRITICAL test_image_insecure
```

| Scanner                                     | Number of vulnerabilities detected                                          |
|---------------------------------------------|-----------------------------------------------------------------------------|
| OSV-Scanner                                 | 3 (severity not reported in default output)                                 | 
| Trivy (a): no filtering                     | 166 (UNKNOWN: 3, LOW: 89, MEDIUM: 23, HIGH: 35, CRITICAL: 16) + 1 (HIGH: 1) |
| Trivy (b): ignore unfixed                   | 45 (UNKNOWN: 3, LOW: 4, MEDIUM: 10, HIGH: 15, CRITICAL: 13)  + 1 (HIGH: 1)  |
| Trivy (c): ignore unfixed, severity >= HIGH | 28 (HIGH: 15, CRITICAL: 13) + 1 (HIGH: 1)                                    |


| Vulnerable library | Trivy | OSV-Scanner           |
|--------------------|--------------------------------------------------------------------------------------------------------------------|--------------------|
| Django (4.1)       | :white_check_mark: | :x:                |
| Openssl            | :white_check_mark: | :white_check_mark: |
| tzdata             | :white_check_mark: | :white_check_mark: |
| many more          | :white_check_mark: | :x:                |

# Conclusions and discussion

## Trivy versus OSV-Scanner
Trivy and OSV-Scanner found the same vulnerability in the lockfile test.

Trivy and OSV-Scanner did not find the same vulnerabilities in the Docker image test.
In particular, Trivy (a) finds many more vulnerabilities (166) than OSV-Scanner (3) 
Of these, trivy finds the same vulnerabilities that OSV-Scanner does.
However, OSV-Scanner does _not_ find the vulnerable Python package (Django 4.1) that Trivy also finds!
This is a deal-breaker for our use-case, as we would like to scan the libraries that are actually installed in the Docker image.

## Making extensive scan results more useful
Trivy is reporting so many vulnerabilities it may be overwhelming for those new to security scanning. 
However, there are a few considerations to take into account:

1. Many of the found vulnerabilities have a relatively low severity
2. Many found vulnerabilities are in low-level libraries and likely won't affect the security the your app in a practical sense
3. Many vulnerabilities have no available fixes, and hence there is nothing _you_ can do about it

Luckily, because Trivy allows the user to configure a minimum severity score and to exclude unfixed (non-actionable) vulnerabilities, you can make your scan results more useful.
If we apply a minimum severity score of "HIGH" and exclude unfixed vulnerabilities, the number of vulnerabilities in the insecure Docker container drop from 166+1 to 28+1.

## A note on reproducibility
The number of found vulnerabilities reported here (date: December 13, 2022) may not be exactly reproducible for two reasons:

1. The vulnerability database is updated daily; new vulnerabilities may be added which were unknown. 
2. The base image that we used for the Docker image (`python:3.10-slim-buster`) may be updated, applying some security patches to vulnerable libraries

# Bonus: Hardening the Docker image
We've seen that the Docker image contains 16 vulnerabilities, 1 of which is the vulnerable version of Django.
We will upgrade Django to a version that has the vulnerability patched (4.1.2).
In addition, we will update the base libraries in the Docker image by running `apt-get update` and `apt-get upgrade` as a `RUN` layer in the `Dockerfile`.

The `requirements.txt` lockfile:
```
Django == 4.1.2
```

The `Dockerfile`:
```Docker
FROM python:3.10-slim-buster

# Update base libraries
RUN \
    apt-get -q update && \
    apt-get upgrade -qq
    
COPY requirements.txt .
RUN pip install -r requirements.txt
```

```bash
docker build . -t test_image_hardened
```

Running trivy again:

```bash
# Test
$ trivy image --security-checks=vuln --ignore-unfixed --severity=HIGH test_image_hardened
```

The output: no vulnerabilities that we can / need to address!
```
2022-12-14T14:07:06.125+0100    INFO    Vulnerability scanning is enabled
2022-12-14T14:07:06.152+0100    INFO    Detected OS: debian
2022-12-14T14:07:06.152+0100    INFO    Detecting Debian vulnerabilities...
2022-12-14T14:07:06.172+0100    INFO    Number of language-specific files: 1
2022-12-14T14:07:06.172+0100    INFO    Detecting python-pkg vulnerabilities...

test_image_hardened (debian 10.13)

Total: 0 (HIGH: 0)
```

# Appendix
The appendix contains the full scan result outputs.

## Lockfile scan - OSV-scan result
```
╭───────────────────────────────┬───────────┬──────────────────┬─────────┬───────────────────────────────────────────────────╮
│ SOURCE                        │ ECOSYSTEM │ AFFECTED PACKAGE │ VERSION │ OSV URL (ID IN BOLD)                              │
├───────────────────────────────┼───────────┼──────────────────┼─────────┼───────────────────────────────────────────────────┤
│ app/insecure/requirements.txt │ PyPI      │ django           │ 4.1     │ https://osv.dev/vulnerability/GHSA-qrw5-5h28-6cmg │
│                               │           │                  │         │ https://osv.dev/vulnerability/PYSEC-2022-304      │
╰───────────────────────────────┴───────────┴──────────────────┴─────────┴───────────────────────────────────────────────────╯
```

## Lockfile scan - Trivy result
``` 
requirements.txt (pip)

Total: 1 (UNKNOWN: 0, LOW: 0, MEDIUM: 0, HIGH: 1, CRITICAL: 0)

┌─────────┬────────────────┬──────────┬───────────────────┬──────────────────────┬─────────────────────────────────────────────────────────────┐
│ Library │ Vulnerability  │ Severity │ Installed Version │    Fixed Version     │                            Title                            │
├─────────┼────────────────┼──────────┼───────────────────┼──────────────────────┼─────────────────────────────────────────────────────────────┤
│ Django  │ CVE-2022-41323 │ HIGH     │ 4.1               │ 3.2.16, 4.0.8, 4.1.2 │ python-django: Potential denial-of-service vulnerability in │
│         │                │          │                   │                      │ internationalized URLs                                      │
│         │                │          │                   │                      │ https://avd.aquasec.com/nvd`/cve-2022-41323                  │
└─────────┴────────────────┴──────────┴───────────────────┴──────────────────────┴─────────────────────────────────────────────────────────────┘
```

## Docker image scan - OSV-Scanner result

```
╭─────────────────────┬───────────┬──────────────────┬──────────────────┬──────────────────────────────────────────╮
│ SOURCE              │ ECOSYSTEM │ AFFECTED PACKAGE │ VERSION          │ OSV URL (ID IN BOLD)                     │
├─────────────────────┼───────────┼──────────────────┼──────────────────┼──────────────────────────────────────────┤
│ test_image_insecure │ Debian    │ openssl          │ 1.1.1n-0+deb10u2 │ https://osv.dev/vulnerability/DSA-5169-1 │
│ test_image_insecure │ Debian    │ tzdata           │ 2021a-0+deb10u4  │ https://osv.dev/vulnerability/DLA-3134-1 │
│ test_image_insecure │ Debian    │ tzdata           │ 2021a-0+deb10u4  │ https://osv.dev/vulnerability/DLA-3161-1 │
╰─────────────────────┴───────────┴──────────────────┴──────────────────┴──────────────────────────────────────────╯
```

## Docker image scan - Trivy (a) result
```python
test_image_insecure (debian 10.12)

Total: 166 (UNKNOWN: 3, LOW: 89, MEDIUM: 23, HIGH: 35, CRITICAL: 16)

┌───────────────┬──────────────────┬──────────┬─────────────────────────┬─────────────────────────┬──────────────────────────────────────────────────────────────┐
│    Library    │  Vulnerability   │ Severity │    Installed Version    │      Fixed Version      │                            Title                             │
├───────────────┼──────────────────┼──────────┼─────────────────────────┼─────────────────────────┼──────────────────────────────────────────────────────────────┤
│ apt           │ CVE-2011-3374    │ LOW      │ 1.8.2.3                 │                         │ It was found that apt-key in apt, all versions, do not       │
│               │                  │          │                         │                         │ correctly...                                                 │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2011-3374                    │
├───────────────┼──────────────────┼──────────┼─────────────────────────┼─────────────────────────┼──────────────────────────────────────────────────────────────┤
│ bash          │ CVE-2022-3715    │ MEDIUM   │ 5.0-4                   │                         │ bash: a heap-buffer-overflow in valid_parameter_transform    │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2022-3715                    │
│               ├──────────────────┼──────────┤                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2019-18276   │ LOW      │                         │                         │ bash: when effective UID is not equal to its real UID the... │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2019-18276                   │
├───────────────┼──────────────────┤          ├─────────────────────────┼─────────────────────────┼──────────────────────────────────────────────────────────────┤
│ bsdutils      │ CVE-2021-37600   │          │ 2.33.1-0.1              │                         │ util-linux: integer overflow can lead to buffer overflow in  │
│               │                  │          │                         │                         │ get_sem_elements() in sys-utils/ipcutils.c...                │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2021-37600                   │
│               ├──────────────────┤          │                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2022-0563    │          │                         │                         │ util-linux: partial disclosure of arbitrary files in chfn    │
│               │                  │          │                         │                         │ and chsh when compiled...                                    │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2022-0563                    │
├───────────────┼──────────────────┤          ├─────────────────────────┼─────────────────────────┼──────────────────────────────────────────────────────────────┤
│ coreutils     │ CVE-2016-2781    │          │ 8.30-3                  │                         │ coreutils: Non-privileged session can escape to the parent   │
│               │                  │          │                         │                         │ session in chroot                                            │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2016-2781                    │
│               ├──────────────────┤          │                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2017-18018   │          │                         │                         │ coreutils: race condition vulnerability in chown and chgrp   │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2017-18018                   │
├───────────────┼──────────────────┼──────────┼─────────────────────────┼─────────────────────────┼──────────────────────────────────────────────────────────────┤
│ e2fsprogs     │ CVE-2022-1304    │ HIGH     │ 1.44.5-1+deb10u3        │                         │ e2fsprogs: out-of-bounds read/write via crafted filesystem   │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2022-1304                    │
├───────────────┼──────────────────┼──────────┼─────────────────────────┼─────────────────────────┼──────────────────────────────────────────────────────────────┤
│ fdisk         │ CVE-2021-37600   │ LOW      │ 2.33.1-0.1              │                         │ util-linux: integer overflow can lead to buffer overflow in  │
│               │                  │          │                         │                         │ get_sem_elements() in sys-utils/ipcutils.c...                │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2021-37600                   │
│               ├──────────────────┤          │                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2022-0563    │          │                         │                         │ util-linux: partial disclosure of arbitrary files in chfn    │
│               │                  │          │                         │                         │ and chsh when compiled...                                    │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2022-0563                    │
├───────────────┼──────────────────┼──────────┼─────────────────────────┼─────────────────────────┼──────────────────────────────────────────────────────────────┤
│ gcc-8-base    │ CVE-2018-12886   │ HIGH     │ 8.3.0-6                 │                         │ gcc: spilling of stack protection address in cfgexpand.c and │
│               │                  │          │                         │                         │ function.c leads to...                                       │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2018-12886                   │
│               ├──────────────────┤          │                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2019-15847   │          │                         │                         │ gcc: POWER9 "DARN" RNG intrinsic produces repeated output    │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2019-15847                   │
├───────────────┼──────────────────┼──────────┼─────────────────────────┼─────────────────────────┼──────────────────────────────────────────────────────────────┤
│ gpgv          │ CVE-2022-34903   │ MEDIUM   │ 2.2.12-1+deb10u1        │ 2.2.12-1+deb10u2        │ gpg: Signature spoofing via status line injection            │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2022-34903                   │
│               ├──────────────────┼──────────┤                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2019-14855   │ LOW      │                         │                         │ gnupg2: OpenPGP Key Certification Forgeries with SHA-1       │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2019-14855                   │
├───────────────┼──────────────────┤          ├─────────────────────────┼─────────────────────────┼──────────────────────────────────────────────────────────────┤
│ libapt-pkg5.0 │ CVE-2011-3374    │          │ 1.8.2.3                 │                         │ It was found that apt-key in apt, all versions, do not       │
│               │                  │          │                         │                         │ correctly...                                                 │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2011-3374                    │
├───────────────┼──────────────────┤          ├─────────────────────────┼─────────────────────────┼──────────────────────────────────────────────────────────────┤
│ libblkid1     │ CVE-2021-37600   │          │ 2.33.1-0.1              │                         │ util-linux: integer overflow can lead to buffer overflow in  │
│               │                  │          │                         │                         │ get_sem_elements() in sys-utils/ipcutils.c...                │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2021-37600                   │
│               ├──────────────────┤          │                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2022-0563    │          │                         │                         │ util-linux: partial disclosure of arbitrary files in chfn    │
│               │                  │          │                         │                         │ and chsh when compiled...                                    │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2022-0563                    │
├───────────────┼──────────────────┼──────────┼─────────────────────────┼─────────────────────────┼──────────────────────────────────────────────────────────────┤
│ libbz2-1.0    │ DLA-3112-1       │ UNKNOWN  │ 1.0.6-9.2~deb10u1       │ 1.0.6-9.2~deb10u2       │ bzip2 - bugfix update                                        │
├───────────────┼──────────────────┼──────────┼─────────────────────────┼─────────────────────────┼──────────────────────────────────────────────────────────────┤
│ libc-bin      │ CVE-2021-33574   │ CRITICAL │ 2.28-10+deb10u1         │ 2.28-10+deb10u2         │ glibc: mq_notify does not handle separately allocated thread │
│               │                  │          │                         │                         │ attributes                                                   │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2021-33574                   │
│               ├──────────────────┤          │                         │                         ├──────────────────────────────────────────────────────────────┤
│               │ CVE-2021-35942   │          │                         │                         │ glibc: Arbitrary read in wordexp()                           │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2021-35942                   │
│               ├──────────────────┤          │                         │                         ├──────────────────────────────────────────────────────────────┤
│               │ CVE-2022-23218   │          │                         │                         │ glibc: Stack-based buffer overflow in svcunix_create via     │
│               │                  │          │                         │                         │ long pathnames                                               │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2022-23218                   │
│               ├──────────────────┤          │                         │                         ├──────────────────────────────────────────────────────────────┤
│               │ CVE-2022-23219   │          │                         │                         │ glibc: Stack-based buffer overflow in sunrpc clnt_create via │
│               │                  │          │                         │                         │ a long pathname                                              │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2022-23219                   │
│               ├──────────────────┼──────────┤                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2020-1751    │ HIGH     │                         │                         │ glibc: array overflow in backtrace functions for powerpc     │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2020-1751                    │
│               ├──────────────────┤          │                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2020-1752    │          │                         │ 2.28-10+deb10u2         │ glibc: use-after-free in glob() function when expanding      │
│               │                  │          │                         │                         │ ~user                                                        │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2020-1752                    │
│               ├──────────────────┤          │                         │                         ├──────────────────────────────────────────────────────────────┤
│               │ CVE-2020-6096    │          │                         │                         │ glibc: signed comparison vulnerability in the ARMv7 memcpy   │
│               │                  │          │                         │                         │ function                                                     │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2020-6096                    │
│               ├──────────────────┤          │                         │                         ├──────────────────────────────────────────────────────────────┤
│               │ CVE-2021-3326    │          │                         │                         │ glibc: Assertion failure in ISO-2022-JP-3 gconv module       │
│               │                  │          │                         │                         │ related to combining characters                              │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2021-3326                    │
│               ├──────────────────┤          │                         │                         ├──────────────────────────────────────────────────────────────┤
│               │ CVE-2021-3999    │          │                         │                         │ glibc: Off-by-one buffer overflow/underflow in getcwd()      │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2021-3999                    │
│               ├──────────────────┼──────────┤                         │                         ├──────────────────────────────────────────────────────────────┤
│               │ CVE-2016-10228   │ MEDIUM   │                         │                         │ glibc: iconv program can hang when invoked with the -c       │
│               │                  │          │                         │                         │ option                                                       │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2016-10228                   │
│               ├──────────────────┤          │                         │                         ├──────────────────────────────────────────────────────────────┤
│               │ CVE-2019-25013   │          │                         │                         │ glibc: buffer over-read in iconv when processing invalid     │
│               │                  │          │                         │                         │ multi-byte input sequences in...                             │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2019-25013                   │
│               ├──────────────────┤          │                         │                         ├──────────────────────────────────────────────────────────────┤
│               │ CVE-2020-10029   │          │                         │                         │ glibc: stack corruption from crafted input in cosl, sinl,    │
│               │                  │          │                         │                         │ sincosl, and tanl...                                         │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2020-10029                   │
│               ├──────────────────┤          │                         │                         ├──────────────────────────────────────────────────────────────┤
│               │ CVE-2020-27618   │          │                         │                         │ glibc: iconv when processing invalid multi-byte input        │
│               │                  │          │                         │                         │ sequences fails to advance the...                            │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2020-27618                   │
│               ├──────────────────┼──────────┤                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2010-4756    │ LOW      │                         │                         │ glibc: glob implementation can cause excessive CPU and       │
│               │                  │          │                         │                         │ memory consumption due to...                                 │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2010-4756                    │
│               ├──────────────────┤          │                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2018-20796   │          │                         │                         │ glibc: uncontrolled recursion in function                    │
│               │                  │          │                         │                         │ check_dst_limits_calc_pos_1 in posix/regexec.c               │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2018-20796                   │
│               ├──────────────────┤          │                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2019-1010022 │          │                         │                         │ glibc: stack guard protection bypass                         │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2019-1010022                 │
│               ├──────────────────┤          │                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2019-1010023 │          │                         │                         │ glibc: running ldd on malicious ELF leads to code execution  │
│               │                  │          │                         │                         │ because of...                                                │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2019-1010023                 │
│               ├──────────────────┤          │                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2019-1010024 │          │                         │                         │ glibc: ASLR bypass using cache of thread stack and heap      │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2019-1010024                 │
│               ├──────────────────┤          │                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2019-1010025 │          │                         │                         │ glibc: information disclosure of heap addresses of           │
│               │                  │          │                         │                         │ pthread_created thread                                       │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2019-1010025                 │
│               ├──────────────────┤          │                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2019-19126   │          │                         │ 2.28-10+deb10u2         │ glibc: LD_PREFER_MAP_32BIT_EXEC not ignored in setuid        │
│               │                  │          │                         │                         │ binaries                                                     │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2019-19126                   │
│               ├──────────────────┤          │                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2019-9192    │          │                         │                         │ glibc: uncontrolled recursion in function                    │
│               │                  │          │                         │                         │ check_dst_limits_calc_pos_1 in posix/regexec.c               │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2019-9192                    │
│               ├──────────────────┤          │                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2021-27645   │          │                         │ 2.28-10+deb10u2         │ glibc: Use-after-free in addgetnetgrentX function in         │
│               │                  │          │                         │                         │ netgroupcache.c                                              │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2021-27645                   │
├───────────────┼──────────────────┼──────────┤                         │                         ├──────────────────────────────────────────────────────────────┤
│ libc6         │ CVE-2021-33574   │ CRITICAL │                         │                         │ glibc: mq_notify does not handle separately allocated thread │
│               │                  │          │                         │                         │ attributes                                                   │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2021-33574                   │
│               ├──────────────────┤          │                         │                         ├──────────────────────────────────────────────────────────────┤
│               │ CVE-2021-35942   │          │                         │                         │ glibc: Arbitrary read in wordexp()                           │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2021-35942                   │
│               ├──────────────────┤          │                         │                         ├──────────────────────────────────────────────────────────────┤
│               │ CVE-2022-23218   │          │                         │                         │ glibc: Stack-based buffer overflow in svcunix_create via     │
│               │                  │          │                         │                         │ long pathnames                                               │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2022-23218                   │
│               ├──────────────────┤          │                         │                         ├──────────────────────────────────────────────────────────────┤
│               │ CVE-2022-23219   │          │                         │                         │ glibc: Stack-based buffer overflow in sunrpc clnt_create via │
│               │                  │          │                         │                         │ a long pathname                                              │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2022-23219                   │
│               ├──────────────────┼──────────┤                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2020-1751    │ HIGH     │                         │                         │ glibc: array overflow in backtrace functions for powerpc     │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2020-1751                    │
│               ├──────────────────┤          │                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2020-1752    │          │                         │ 2.28-10+deb10u2         │ glibc: use-after-free in glob() function when expanding      │
│               │                  │          │                         │                         │ ~user                                                        │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2020-1752                    │
│               ├──────────────────┤          │                         │                         ├──────────────────────────────────────────────────────────────┤
│               │ CVE-2020-6096    │          │                         │                         │ glibc: signed comparison vulnerability in the ARMv7 memcpy   │
│               │                  │          │                         │                         │ function                                                     │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2020-6096                    │
│               ├──────────────────┤          │                         │                         ├──────────────────────────────────────────────────────────────┤
│               │ CVE-2021-3326    │          │                         │                         │ glibc: Assertion failure in ISO-2022-JP-3 gconv module       │
│               │                  │          │                         │                         │ related to combining characters                              │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2021-3326                    │
│               ├──────────────────┤          │                         │                         ├──────────────────────────────────────────────────────────────┤
│               │ CVE-2021-3999    │          │                         │                         │ glibc: Off-by-one buffer overflow/underflow in getcwd()      │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2021-3999                    │
│               ├──────────────────┼──────────┤                         │                         ├──────────────────────────────────────────────────────────────┤
│               │ CVE-2016-10228   │ MEDIUM   │                         │                         │ glibc: iconv program can hang when invoked with the -c       │
│               │                  │          │                         │                         │ option                                                       │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2016-10228                   │
│               ├──────────────────┤          │                         │                         ├──────────────────────────────────────────────────────────────┤
│               │ CVE-2019-25013   │          │                         │                         │ glibc: buffer over-read in iconv when processing invalid     │
│               │                  │          │                         │                         │ multi-byte input sequences in...                             │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2019-25013                   │
│               ├──────────────────┤          │                         │                         ├──────────────────────────────────────────────────────────────┤
│               │ CVE-2020-10029   │          │                         │                         │ glibc: stack corruption from crafted input in cosl, sinl,    │
│               │                  │          │                         │                         │ sincosl, and tanl...                                         │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2020-10029                   │
│               ├──────────────────┤          │                         │                         ├──────────────────────────────────────────────────────────────┤
│               │ CVE-2020-27618   │          │                         │                         │ glibc: iconv when processing invalid multi-byte input        │
│               │                  │          │                         │                         │ sequences fails to advance the...                            │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2020-27618                   │
│               ├──────────────────┼──────────┤                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2010-4756    │ LOW      │                         │                         │ glibc: glob implementation can cause excessive CPU and       │
│               │                  │          │                         │                         │ memory consumption due to...                                 │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2010-4756                    │
│               ├──────────────────┤          │                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2018-20796   │          │                         │                         │ glibc: uncontrolled recursion in function                    │
│               │                  │          │                         │                         │ check_dst_limits_calc_pos_1 in posix/regexec.c               │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2018-20796                   │
│               ├──────────────────┤          │                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2019-1010022 │          │                         │                         │ glibc: stack guard protection bypass                         │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2019-1010022                 │
│               ├──────────────────┤          │                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2019-1010023 │          │                         │                         │ glibc: running ldd on malicious ELF leads to code execution  │
│               │                  │          │                         │                         │ because of...                                                │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2019-1010023                 │
│               ├──────────────────┤          │                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2019-1010024 │          │                         │                         │ glibc: ASLR bypass using cache of thread stack and heap      │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2019-1010024                 │
│               ├──────────────────┤          │                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2019-1010025 │          │                         │                         │ glibc: information disclosure of heap addresses of           │
│               │                  │          │                         │                         │ pthread_created thread                                       │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2019-1010025                 │
│               ├──────────────────┤          │                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2019-19126   │          │                         │ 2.28-10+deb10u2         │ glibc: LD_PREFER_MAP_32BIT_EXEC not ignored in setuid        │
│               │                  │          │                         │                         │ binaries                                                     │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2019-19126                   │
│               ├──────────────────┤          │                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2019-9192    │          │                         │                         │ glibc: uncontrolled recursion in function                    │
│               │                  │          │                         │                         │ check_dst_limits_calc_pos_1 in posix/regexec.c               │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2019-9192                    │
│               ├──────────────────┤          │                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2021-27645   │          │                         │ 2.28-10+deb10u2         │ glibc: Use-after-free in addgetnetgrentX function in         │
│               │                  │          │                         │                         │ netgroupcache.c                                              │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2021-27645                   │
├───────────────┼──────────────────┼──────────┼─────────────────────────┼─────────────────────────┼──────────────────────────────────────────────────────────────┤
│ libcom-err2   │ CVE-2022-1304    │ HIGH     │ 1.44.5-1+deb10u3        │                         │ e2fsprogs: out-of-bounds read/write via crafted filesystem   │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2022-1304                    │
├───────────────┼──────────────────┼──────────┼─────────────────────────┼─────────────────────────┼──────────────────────────────────────────────────────────────┤
│ libdb5.3      │ CVE-2019-8457    │ CRITICAL │ 5.3.28+dfsg1-0.5        │                         │ sqlite: heap out-of-bound read in function rtreenode()       │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2019-8457                    │
├───────────────┼──────────────────┤          ├─────────────────────────┼─────────────────────────┼──────────────────────────────────────────────────────────────┤
│ libexpat1     │ CVE-2022-40674   │          │ 2.2.6-2+deb10u4         │ 2.2.6-2+deb10u5         │ expat: a use-after-free in the doContent function in         │
│               │                  │          │                         │                         │ xmlparse.c                                                   │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2022-40674                   │
│               ├──────────────────┼──────────┤                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2022-43680   │ HIGH     │                         │ 2.2.6-2+deb10u6         │ expat: use-after free caused by overeager destruction of a   │
│               │                  │          │                         │                         │ shared DTD in...                                             │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2022-43680                   │
│               ├──────────────────┼──────────┤                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2013-0340    │ LOW      │                         │                         │ expat: internal entity expansion                             │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2013-0340                    │
├───────────────┼──────────────────┼──────────┼─────────────────────────┼─────────────────────────┼──────────────────────────────────────────────────────────────┤
│ libext2fs2    │ CVE-2022-1304    │ HIGH     │ 1.44.5-1+deb10u3        │                         │ e2fsprogs: out-of-bounds read/write via crafted filesystem   │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2022-1304                    │
├───────────────┼──────────────────┼──────────┼─────────────────────────┼─────────────────────────┼──────────────────────────────────────────────────────────────┤
│ libfdisk1     │ CVE-2021-37600   │ LOW      │ 2.33.1-0.1              │                         │ util-linux: integer overflow can lead to buffer overflow in  │
│               │                  │          │                         │                         │ get_sem_elements() in sys-utils/ipcutils.c...                │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2021-37600                   │
│               ├──────────────────┤          │                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2022-0563    │          │                         │                         │ util-linux: partial disclosure of arbitrary files in chfn    │
│               │                  │          │                         │                         │ and chsh when compiled...                                    │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2022-0563                    │
├───────────────┼──────────────────┼──────────┼─────────────────────────┼─────────────────────────┼──────────────────────────────────────────────────────────────┤
│ libgcc1       │ CVE-2018-12886   │ HIGH     │ 8.3.0-6                 │                         │ gcc: spilling of stack protection address in cfgexpand.c and │
│               │                  │          │                         │                         │ function.c leads to...                                       │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2018-12886                   │
│               ├──────────────────┤          │                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2019-15847   │          │                         │                         │ gcc: POWER9 "DARN" RNG intrinsic produces repeated output    │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2019-15847                   │
├───────────────┼──────────────────┤          ├─────────────────────────┼─────────────────────────┼──────────────────────────────────────────────────────────────┤
│ libgcrypt20   │ CVE-2021-33560   │          │ 1.8.4-5+deb10u1         │                         │ libgcrypt: mishandles ElGamal encryption because it lacks    │
│               │                  │          │                         │                         │ exponent blinding to address a...                            │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2021-33560                   │
│               ├──────────────────┼──────────┤                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2019-13627   │ MEDIUM   │                         │                         │ libgcrypt: ECDSA timing attack allowing private key leak     │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2019-13627                   │
│               ├──────────────────┼──────────┤                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2018-6829    │ LOW      │                         │                         │ libgcrypt: ElGamal implementation doesn't have semantic      │
│               │                  │          │                         │                         │ security due to incorrectly encoded plaintexts...            │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2018-6829                    │
├───────────────┼──────────────────┼──────────┼─────────────────────────┼─────────────────────────┼──────────────────────────────────────────────────────────────┤
│ libgnutls30   │ CVE-2022-2509    │ HIGH     │ 3.6.7-4+deb10u7         │ 3.6.7-4+deb10u9         │ gnutls: Double free during gnutls_pkcs7_verify               │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2022-2509                    │
│               ├──────────────────┼──────────┤                         │                         ├──────────────────────────────────────────────────────────────┤
│               │ CVE-2021-4209    │ MEDIUM   │                         │                         │ GnuTLS: Null pointer dereference in MD_UPDATE                │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2021-4209                    │
│               ├──────────────────┼──────────┤                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2011-3389    │ LOW      │                         │                         │ HTTPS: block-wise chosen-plaintext attack against SSL/TLS    │
│               │                  │          │                         │                         │ (BEAST)                                                      │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2011-3389                    │
├───────────────┼──────────────────┼──────────┼─────────────────────────┼─────────────────────────┼──────────────────────────────────────────────────────────────┤
│ libidn2-0     │ CVE-2019-12290   │ HIGH     │ 2.0.5-1+deb10u1         │                         │ GNU libidn2 before 2.2.0 fails to perform the roundtrip      │
│               │                  │          │                         │                         │ checks specifi ......                                        │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2019-12290                   │
├───────────────┼──────────────────┼──────────┼─────────────────────────┼─────────────────────────┼──────────────────────────────────────────────────────────────┤
│ liblz4-1      │ CVE-2019-17543   │ LOW      │ 1.8.3-1+deb10u1         │                         │ lz4: heap-based buffer overflow in LZ4_write32               │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2019-17543                   │
├───────────────┼──────────────────┤          ├─────────────────────────┼─────────────────────────┼──────────────────────────────────────────────────────────────┤
│ libmount1     │ CVE-2021-37600   │          │ 2.33.1-0.1              │                         │ util-linux: integer overflow can lead to buffer overflow in  │
│               │                  │          │                         │                         │ get_sem_elements() in sys-utils/ipcutils.c...                │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2021-37600                   │
│               ├──────────────────┤          │                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2022-0563    │          │                         │                         │ util-linux: partial disclosure of arbitrary files in chfn    │
│               │                  │          │                         │                         │ and chsh when compiled...                                    │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2022-0563                    │
├───────────────┼──────────────────┼──────────┼─────────────────────────┼─────────────────────────┼──────────────────────────────────────────────────────────────┤
│ libncursesw6  │ CVE-2022-29458   │ HIGH     │ 6.1+20181013-2+deb10u2  │ 6.1+20181013-2+deb10u3  │ ncurses: segfaulting OOB read                                │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2022-29458                   │
│               ├──────────────────┼──────────┤                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2021-39537   │ LOW      │                         │                         │ ncurses: heap-based buffer overflow in _nc_captoinfo() in    │
│               │                  │          │                         │                         │ captoinfo.c                                                  │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2021-39537                   │
├───────────────┼──────────────────┼──────────┼─────────────────────────┼─────────────────────────┼──────────────────────────────────────────────────────────────┤
│ libpcre3      │ CVE-2020-14155   │ MEDIUM   │ 2:8.39-12               │                         │ pcre: Integer overflow when parsing callout numeric          │
│               │                  │          │                         │                         │ arguments                                                    │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2020-14155                   │
│               ├──────────────────┼──────────┤                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2017-11164   │ LOW      │                         │                         │ pcre: OP_KETRMAX feature in the match function in            │
│               │                  │          │                         │                         │ pcre_exec.c                                                  │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2017-11164                   │
│               ├──────────────────┤          │                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2017-16231   │          │                         │                         │ pcre: self-recursive call in match() in pcre_exec.c leads to │
│               │                  │          │                         │                         │ denial of service...                                         │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2017-16231                   │
│               ├──────────────────┤          │                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2017-7245    │          │                         │                         │ pcre: stack-based buffer overflow write in                   │
│               │                  │          │                         │                         │ pcre32_copy_substring                                        │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2017-7245                    │
│               ├──────────────────┤          │                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2017-7246    │          │                         │                         │ pcre: stack-based buffer overflow write in                   │
│               │                  │          │                         │                         │ pcre32_copy_substring                                        │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2017-7246                    │
│               ├──────────────────┤          │                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2019-20838   │          │                         │                         │ pcre: Buffer over-read in JIT when UTF is disabled and \X    │
│               │                  │          │                         │                         │ or...                                                        │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2019-20838                   │
├───────────────┼──────────────────┤          ├─────────────────────────┼─────────────────────────┼──────────────────────────────────────────────────────────────┤
│ libseccomp2   │ CVE-2019-9893    │          │ 2.3.3-4                 │                         │ libseccomp: incorrect generation of syscall filters in       │
│               │                  │          │                         │                         │ libseccomp                                                   │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2019-9893                    │
├───────────────┼──────────────────┤          ├─────────────────────────┼─────────────────────────┼──────────────────────────────────────────────────────────────┤
│ libsepol1     │ CVE-2021-36084   │          │ 2.8-1                   │                         │ libsepol: use-after-free in __cil_verify_classperms()        │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2021-36084                   │
│               ├──────────────────┤          │                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2021-36085   │          │                         │                         │ libsepol: use-after-free in __cil_verify_classperms()        │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2021-36085                   │
│               ├──────────────────┤          │                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2021-36086   │          │                         │                         │ libsepol: use-after-free in cil_reset_classpermission()      │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2021-36086                   │
│               ├──────────────────┤          │                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2021-36087   │          │                         │                         │ libsepol: heap-based buffer overflow in ebitmap_match_any()  │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2021-36087                   │
├───────────────┼──────────────────┤          ├─────────────────────────┼─────────────────────────┼──────────────────────────────────────────────────────────────┤
│ libsmartcols1 │ CVE-2021-37600   │          │ 2.33.1-0.1              │                         │ util-linux: integer overflow can lead to buffer overflow in  │
│               │                  │          │                         │                         │ get_sem_elements() in sys-utils/ipcutils.c...                │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2021-37600                   │
│               ├──────────────────┤          │                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2022-0563    │          │                         │                         │ util-linux: partial disclosure of arbitrary files in chfn    │
│               │                  │          │                         │                         │ and chsh when compiled...                                    │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2022-0563                    │
├───────────────┼──────────────────┼──────────┼─────────────────────────┼─────────────────────────┼──────────────────────────────────────────────────────────────┤
│ libsqlite3-0  │ CVE-2020-35527   │ CRITICAL │ 3.27.2-3+deb10u1        │ 3.27.2-3+deb10u2        │ sqlite: Out of bounds access during table rename             │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2020-35527                   │
│               ├──────────────────┤          │                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2022-46908   │          │                         │                         │ sqlite: safe mode authorizer callback allows disallowed UDFs │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2022-46908                   │
│               ├──────────────────┼──────────┤                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2019-19603   │ HIGH     │                         │                         │ sqlite: mishandling of certain SELECT statements with        │
│               │                  │          │                         │                         │ non-existent VIEW can lead to...                             │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2019-19603                   │
│               ├──────────────────┤          │                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2020-35525   │          │                         │ 3.27.2-3+deb10u2        │ sqlite: Null pointer derreference in src/select.c            │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2020-35525                   │
│               ├──────────────────┼──────────┤                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2019-19645   │ MEDIUM   │                         │                         │ sqlite: infinite recursion via certain types of              │
│               │                  │          │                         │                         │ self-referential views in conjunction with...                │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2019-19645                   │
│               ├──────────────────┤          │                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2019-19924   │          │                         │                         │ sqlite: incorrect sqlite3WindowRewrite() error handling      │
│               │                  │          │                         │                         │ leads to mishandling certain parser-tree rewriting           │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2019-19924                   │
│               ├──────────────────┤          │                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2020-13631   │          │                         │                         │ sqlite: Virtual table can be renamed into the name of one    │
│               │                  │          │                         │                         │ of...                                                        │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2020-13631                   │
│               ├──────────────────┤          │                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2021-45346   │          │                         │                         │ sqlite: crafted SQL query allows a malicious user to obtain  │
│               │                  │          │                         │                         │ sensitive information...                                     │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2021-45346                   │
│               ├──────────────────┼──────────┤                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2019-19244   │ LOW      │                         │                         │ sqlite: allows a crash if a sub-select uses both DISTINCT    │
│               │                  │          │                         │                         │ and window...                                                │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2019-19244                   │
│               ├──────────────────┤          │                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2020-11656   │          │                         │                         │ sqlite: use-after-free in the ALTER TABLE implementation     │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2020-11656                   │
│               ├──────────────────┤          │                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2021-36690   │          │                         │                         │ ** DISPUTED ** A segmentation fault can occur in the         │
│               │                  │          │                         │                         │ sqlite3.exe comma...                                         │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2021-36690                   │
│               ├──────────────────┤          │                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2022-35737   │          │                         │                         │ sqlite: an array-bounds overflow if billions of bytes are    │
│               │                  │          │                         │                         │ used in a...                                                 │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2022-35737                   │
├───────────────┼──────────────────┼──────────┼─────────────────────────┼─────────────────────────┼──────────────────────────────────────────────────────────────┤
│ libss2        │ CVE-2022-1304    │ HIGH     │ 1.44.5-1+deb10u3        │                         │ e2fsprogs: out-of-bounds read/write via crafted filesystem   │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2022-1304                    │
├───────────────┼──────────────────┼──────────┼─────────────────────────┼─────────────────────────┼──────────────────────────────────────────────────────────────┤
│ libssl1.1     │ CVE-2022-2068    │ CRITICAL │ 1.1.1n-0+deb10u2        │ 1.1.1n-0+deb10u3        │ openssl: the c_rehash script allows command injection        │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2022-2068                    │
│               ├──────────────────┼──────────┤                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2022-2097    │ MEDIUM   │                         │                         │ openssl: AES OCB fails to encrypt some bytes                 │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2022-2097                    │
│               ├──────────────────┼──────────┤                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2007-6755    │ LOW      │                         │                         │ Dual_EC_DRBG: weak pseudo random number generator            │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2007-6755                    │
│               ├──────────────────┤          │                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2010-0928    │          │                         │                         │ openssl: RSA authentication weakness                         │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2010-0928                    │
├───────────────┼──────────────────┼──────────┼─────────────────────────┼─────────────────────────┼──────────────────────────────────────────────────────────────┤
│ libstdc++6    │ CVE-2018-12886   │ HIGH     │ 8.3.0-6                 │                         │ gcc: spilling of stack protection address in cfgexpand.c and │
│               │                  │          │                         │                         │ function.c leads to...                                       │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2018-12886                   │
│               ├──────────────────┤          │                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2019-15847   │          │                         │                         │ gcc: POWER9 "DARN" RNG intrinsic produces repeated output    │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2019-15847                   │
├───────────────┼──────────────────┤          ├─────────────────────────┼─────────────────────────┼──────────────────────────────────────────────────────────────┤
│ libsystemd0   │ CVE-2019-3843    │          │ 241-7~deb10u8           │                         │ systemd: services with DynamicUser can create SUID/SGID      │
│               │                  │          │                         │                         │ binaries                                                     │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2019-3843                    │
│               ├──────────────────┤          │                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2019-3844    │          │                         │                         │ systemd: services with DynamicUser can get new privileges    │
│               │                  │          │                         │                         │ and create SGID binaries...                                  │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2019-3844                    │
│               ├──────────────────┼──────────┤                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2021-3997    │ MEDIUM   │                         │                         │ systemd: Uncontrolled recursion in systemd-tmpfiles when     │
│               │                  │          │                         │                         │ removing files                                               │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2021-3997                    │
│               ├──────────────────┤          │                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2022-3821    │          │                         │                         │ systemd: buffer overrun in format_timespan() function        │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2022-3821                    │
│               ├──────────────────┼──────────┤                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2013-4392    │ LOW      │                         │                         │ systemd: TOCTOU race condition when updating file            │
│               │                  │          │                         │                         │ permissions and SELinux security contexts...                 │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2013-4392                    │
│               ├──────────────────┤          │                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2019-20386   │          │                         │                         │ systemd: memory leak in button_open() in                     │
│               │                  │          │                         │                         │ login/logind-button.c when udev events are received...       │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2019-20386                   │
│               ├──────────────────┤          │                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2020-13529   │          │                         │                         │ systemd: DHCP FORCERENEW authentication not implemented can  │
│               │                  │          │                         │                         │ cause a system running the...                                │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2020-13529                   │
│               ├──────────────────┤          │                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2020-13776   │          │                         │                         │ systemd: Mishandles numerical usernames beginning with       │
│               │                  │          │                         │                         │ decimal digits or 0x followed by...                          │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2020-13776                   │
├───────────────┼──────────────────┼──────────┼─────────────────────────┼─────────────────────────┼──────────────────────────────────────────────────────────────┤
│ libtasn1-6    │ CVE-2021-46848   │ CRITICAL │ 4.13-3                  │                         │ libtasn1: Out-of-bound access in ETYPE_OK                    │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2021-46848                   │
│               ├──────────────────┼──────────┤                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2018-1000654 │ LOW      │                         │                         │ libtasn1: Infinite loop in _asn1_expand_object_id(ptree)     │
│               │                  │          │                         │                         │ leads to memory exhaustion                                   │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2018-1000654                 │
├───────────────┼──────────────────┼──────────┼─────────────────────────┼─────────────────────────┼──────────────────────────────────────────────────────────────┤
│ libtinfo6     │ CVE-2022-29458   │ HIGH     │ 6.1+20181013-2+deb10u2  │ 6.1+20181013-2+deb10u3  │ ncurses: segfaulting OOB read                                │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2022-29458                   │
│               ├──────────────────┼──────────┤                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2021-39537   │ LOW      │                         │                         │ ncurses: heap-based buffer overflow in _nc_captoinfo() in    │
│               │                  │          │                         │                         │ captoinfo.c                                                  │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2021-39537                   │
├───────────────┼──────────────────┼──────────┼─────────────────────────┼─────────────────────────┼──────────────────────────────────────────────────────────────┤
│ libudev1      │ CVE-2019-3843    │ HIGH     │ 241-7~deb10u8           │                         │ systemd: services with DynamicUser can create SUID/SGID      │
│               │                  │          │                         │                         │ binaries                                                     │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2019-3843                    │
│               ├──────────────────┤          │                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2019-3844    │          │                         │                         │ systemd: services with DynamicUser can get new privileges    │
│               │                  │          │                         │                         │ and create SGID binaries...                                  │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2019-3844                    │
│               ├──────────────────┼──────────┤                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2021-3997    │ MEDIUM   │                         │                         │ systemd: Uncontrolled recursion in systemd-tmpfiles when     │
│               │                  │          │                         │                         │ removing files                                               │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2021-3997                    │
│               ├──────────────────┤          │                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2022-3821    │          │                         │                         │ systemd: buffer overrun in format_timespan() function        │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2022-3821                    │
│               ├──────────────────┼──────────┤                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2013-4392    │ LOW      │                         │                         │ systemd: TOCTOU race condition when updating file            │
│               │                  │          │                         │                         │ permissions and SELinux security contexts...                 │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2013-4392                    │
│               ├──────────────────┤          │                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2019-20386   │          │                         │                         │ systemd: memory leak in button_open() in                     │
│               │                  │          │                         │                         │ login/logind-button.c when udev events are received...       │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2019-20386                   │
│               ├──────────────────┤          │                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2020-13529   │          │                         │                         │ systemd: DHCP FORCERENEW authentication not implemented can  │
│               │                  │          │                         │                         │ cause a system running the...                                │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2020-13529                   │
│               ├──────────────────┤          │                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2020-13776   │          │                         │                         │ systemd: Mishandles numerical usernames beginning with       │
│               │                  │          │                         │                         │ decimal digits or 0x followed by...                          │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2020-13776                   │
├───────────────┼──────────────────┤          ├─────────────────────────┼─────────────────────────┼──────────────────────────────────────────────────────────────┤
│ libuuid1      │ CVE-2021-37600   │          │ 2.33.1-0.1              │                         │ util-linux: integer overflow can lead to buffer overflow in  │
│               │                  │          │                         │                         │ get_sem_elements() in sys-utils/ipcutils.c...                │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2021-37600                   │
│               ├──────────────────┤          │                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2022-0563    │          │                         │                         │ util-linux: partial disclosure of arbitrary files in chfn    │
│               │                  │          │                         │                         │ and chsh when compiled...                                    │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2022-0563                    │
├───────────────┼──────────────────┤          ├─────────────────────────┼─────────────────────────┼──────────────────────────────────────────────────────────────┤
│ login         │ CVE-2007-5686    │          │ 1:4.5-1.1               │                         │ initscripts in rPath Linux 1 sets insecure permissions for   │
│               │                  │          │                         │                         │ the /var/lo ......                                           │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2007-5686                    │
│               ├──────────────────┤          │                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2013-4235    │          │                         │                         │ shadow-utils: TOCTOU race conditions by copying and removing │
│               │                  │          │                         │                         │ directory trees                                              │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2013-4235                    │
│               ├──────────────────┤          │                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2018-7169    │          │                         │                         │ shadow-utils: newgidmap allows unprivileged user to drop     │
│               │                  │          │                         │                         │ supplementary groups potentially allowing privilege...       │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2018-7169                    │
│               ├──────────────────┤          │                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2019-19882   │          │                         │                         │ shadow-utils: local users can obtain root access because     │
│               │                  │          │                         │                         │ setuid programs are misconfigured...                         │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2019-19882                   │
├───────────────┼──────────────────┤          ├─────────────────────────┼─────────────────────────┼──────────────────────────────────────────────────────────────┤
│ mount         │ CVE-2021-37600   │          │ 2.33.1-0.1              │                         │ util-linux: integer overflow can lead to buffer overflow in  │
│               │                  │          │                         │                         │ get_sem_elements() in sys-utils/ipcutils.c...                │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2021-37600                   │
│               ├──────────────────┤          │                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2022-0563    │          │                         │                         │ util-linux: partial disclosure of arbitrary files in chfn    │
│               │                  │          │                         │                         │ and chsh when compiled...                                    │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2022-0563                    │
├───────────────┼──────────────────┼──────────┼─────────────────────────┼─────────────────────────┼──────────────────────────────────────────────────────────────┤
│ ncurses-base  │ CVE-2022-29458   │ HIGH     │ 6.1+20181013-2+deb10u2  │ 6.1+20181013-2+deb10u3  │ ncurses: segfaulting OOB read                                │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2022-29458                   │
│               ├──────────────────┼──────────┤                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2021-39537   │ LOW      │                         │                         │ ncurses: heap-based buffer overflow in _nc_captoinfo() in    │
│               │                  │          │                         │                         │ captoinfo.c                                                  │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2021-39537                   │
├───────────────┼──────────────────┼──────────┤                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│ ncurses-bin   │ CVE-2022-29458   │ HIGH     │                         │ 6.1+20181013-2+deb10u3  │ ncurses: segfaulting OOB read                                │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2022-29458                   │
│               ├──────────────────┼──────────┤                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2021-39537   │ LOW      │                         │                         │ ncurses: heap-based buffer overflow in _nc_captoinfo() in    │
│               │                  │          │                         │                         │ captoinfo.c                                                  │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2021-39537                   │
├───────────────┼──────────────────┼──────────┼─────────────────────────┼─────────────────────────┼──────────────────────────────────────────────────────────────┤
│ openssl       │ CVE-2022-2068    │ CRITICAL │ 1.1.1n-0+deb10u2        │ 1.1.1n-0+deb10u3        │ openssl: the c_rehash script allows command injection        │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2022-2068                    │
│               ├──────────────────┼──────────┤                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2022-2097    │ MEDIUM   │                         │                         │ openssl: AES OCB fails to encrypt some bytes                 │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2022-2097                    │
│               ├──────────────────┼──────────┤                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2007-6755    │ LOW      │                         │                         │ Dual_EC_DRBG: weak pseudo random number generator            │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2007-6755                    │
│               ├──────────────────┤          │                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2010-0928    │          │                         │                         │ openssl: RSA authentication weakness                         │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2010-0928                    │
├───────────────┼──────────────────┤          ├─────────────────────────┼─────────────────────────┼──────────────────────────────────────────────────────────────┤
│ passwd        │ CVE-2007-5686    │          │ 1:4.5-1.1               │                         │ initscripts in rPath Linux 1 sets insecure permissions for   │
│               │                  │          │                         │                         │ the /var/lo ......                                           │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2007-5686                    │
│               ├──────────────────┤          │                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2013-4235    │          │                         │                         │ shadow-utils: TOCTOU race conditions by copying and removing │
│               │                  │          │                         │                         │ directory trees                                              │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2013-4235                    │
│               ├──────────────────┤          │                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2018-7169    │          │                         │                         │ shadow-utils: newgidmap allows unprivileged user to drop     │
│               │                  │          │                         │                         │ supplementary groups potentially allowing privilege...       │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2018-7169                    │
│               ├──────────────────┤          │                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2019-19882   │          │                         │                         │ shadow-utils: local users can obtain root access because     │
│               │                  │          │                         │                         │ setuid programs are misconfigured...                         │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2019-19882                   │
├───────────────┼──────────────────┼──────────┼─────────────────────────┼─────────────────────────┼──────────────────────────────────────────────────────────────┤
│ perl-base     │ CVE-2020-16156   │ HIGH     │ 5.28.1-6+deb10u1        │                         │ perl-CPAN: Bypass of verification of signatures in CHECKSUMS │
│               │                  │          │                         │                         │ files                                                        │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2020-16156                   │
│               ├──────────────────┼──────────┤                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2011-4116    │ LOW      │                         │                         │ perl: File::Temp insecure temporary file handling            │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2011-4116                    │
├───────────────┼──────────────────┤          ├─────────────────────────┼─────────────────────────┼──────────────────────────────────────────────────────────────┤
│ tar           │ CVE-2005-2541    │          │ 1.30+dfsg-6             │                         │ tar: does not properly warn the user when extracting setuid  │
│               │                  │          │                         │                         │ or setgid...                                                 │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2005-2541                    │
│               ├──────────────────┤          │                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2019-9923    │          │                         │                         │ tar: null-pointer dereference in pax_decode_header in        │
│               │                  │          │                         │                         │ sparse.c                                                     │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2019-9923                    │
│               ├──────────────────┤          │                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2021-20193   │          │                         │                         │ tar: Memory leak in read_header() in list.c                  │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2021-20193                   │
├───────────────┼──────────────────┼──────────┼─────────────────────────┼─────────────────────────┼──────────────────────────────────────────────────────────────┤
│ tzdata        │ DLA-3134-1       │ UNKNOWN  │ 2021a-0+deb10u4         │ 2021a-0+deb10u7         │ tzdata - new timezone database                               │
│               ├──────────────────┤          │                         ├─────────────────────────┤                                                              │
│               │ DLA-3161-1       │          │                         │ 2021a-0+deb10u8         │                                                              │
├───────────────┼──────────────────┼──────────┼─────────────────────────┼─────────────────────────┼──────────────────────────────────────────────────────────────┤
│ util-linux    │ CVE-2021-37600   │ LOW      │ 2.33.1-0.1              │                         │ util-linux: integer overflow can lead to buffer overflow in  │
│               │                  │          │                         │                         │ get_sem_elements() in sys-utils/ipcutils.c...                │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2021-37600                   │
│               ├──────────────────┤          │                         ├─────────────────────────┼──────────────────────────────────────────────────────────────┤
│               │ CVE-2022-0563    │          │                         │                         │ util-linux: partial disclosure of arbitrary files in chfn    │
│               │                  │          │                         │                         │ and chsh when compiled...                                    │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2022-0563                    │
├───────────────┼──────────────────┼──────────┼─────────────────────────┼─────────────────────────┼──────────────────────────────────────────────────────────────┤
│ zlib1g        │ CVE-2022-37434   │ CRITICAL │ 1:1.2.11.dfsg-1+deb10u1 │ 1:1.2.11.dfsg-1+deb10u2 │ zlib: heap-based buffer over-read and overflow in inflate()  │
│               │                  │          │                         │                         │ in inflate.c via a...                                        │
│               │                  │          │                         │                         │ https://avd.aquasec.com/nvd/cve-2022-37434                   │
└───────────────┴──────────────────┴──────────┴─────────────────────────┴─────────────────────────┴──────────────────────────────────────────────────────────────┘
2022-12-14T12:47:57.311+0100    INFO    Table result includes only package filenames. Use '--format json' option to get the full path to the package file.

Python (python-pkg)

Total: 1 (UNKNOWN: 0, LOW: 0, MEDIUM: 0, HIGH: 1, CRITICAL: 0)

┌───────────────────┬────────────────┬──────────┬───────────────────┬──────────────────────┬─────────────────────────────────────────────────────────────┐
│      Library      │ Vulnerability  │ Severity │ Installed Version │    Fixed Version     │                            Title                            │
├───────────────────┼────────────────┼──────────┼───────────────────┼──────────────────────┼─────────────────────────────────────────────────────────────┤
│ Django (METADATA) │ CVE-2022-41323 │ HIGH     │ 4.1               │ 3.2.16, 4.0.8, 4.1.2 │ python-django: Potential denial-of-service vulnerability in │
│                   │                │          │                   │                      │ internationalized URLs                                      │
│                   │                │          │                   │                      │ https://avd.aquasec.com/nvd/cve-2022-41323                  │
└───────────────────┴────────────────┴──────────┴───────────────────┴──────────────────────┴─────────────────────────────────────────────────────────────┘
```
