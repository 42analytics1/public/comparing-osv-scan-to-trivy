IMAGE_INSECURE = test_image_insecure
IMAGE_SECURE = test_image_secure
OSV = ~/go/bin/osv-scanner

.ONESHELL:

.SHELLFLAGS := -euo pipefail -c
SHELL := bash

build_secure:
	cd app/secure
	docker build . -t ${IMAGE_SECURE}

build_insecure:
	cd app/insecure
	docker build . -t ${IMAGE_INSECURE}

build: build_secure build_insecure
	@echo "Done"

osv-lockfile:
	${OSV} -L app/insecure/requirements.txt

trivy-lockfile:
	trivy filesystem --security-checks=vuln app/insecure

osv-docker:
	${OSV} --docker ${IMAGE_INSECURE}

trivy-docker:
	trivy image --security-checks=vuln ${IMAGE_INSECURE}

trivy-docker-reduced:
	trivy image --severity HIGH --ignore-unfixed --security-checks=vuln ${IMAGE_INSECURE}

trivy-docker-hardened:
	trivy image --severity HIGH --ignore-unfixed --security-checks=vuln ${IMAGE_SECURE}

